console.log("Hello World");

let firstName = 'John';
const firstNameGreeting = 'First Name: ' + firstName;
console.log(firstNameGreeting);

let lastName = 'Smith';
const lastNameGreeting = 'Last Name: ' + lastName;
console.log(lastNameGreeting);

let age = 30;
const ageGreeting = 'Age: ' + age;
console.log(ageGreeting);

let hobbies = "";
const hobbiesGreeting = 'Hobbies: ' + hobbies;
console.log(hobbiesGreeting);

hobbies = ['Biking', 'Mountain Climbing', 'Swimming'];
console.log(hobbies);

const workAddressGreeting = 'Work Address: ';
console.log(workAddressGreeting);

let workAddress = {
	houseNumber: '32',
	street: 'Washington',
	city: 'Lincoln',
	state: 'Nebraska'
};
console.log(workAddress);


firstName = 'Steve';
lastName = 'Rogers';
let fullName = firstName + " " + lastName;

let fullNameGreeting = 'My full name is: ' + fullName;
console.log(fullNameGreeting);

age = 40;
let fullAgeGreeting = 'My current age is: ' + age;
console.log(fullAgeGreeting);

let friends = "";
const friendsGreeting = 'My Friends are: ' + friends;
console.log(friendsGreeting);

friends = ['Tony', 'Bruce', 'Thor', 'Natasha', 'Clint', 'Nick'];
console.log(friends);

let fullProfile = "";

const fullProfileGreeting = 'My Full Profile: ' + fullProfile;
console.log(fullProfileGreeting);

fullProfile = {
	username: 'captain_america',
	fullName: fullName,
	age: age,
	isActive: false
};
console.log(fullProfile);

friends = 'Bucky Barnes';
const bestfriendGreeting = 'My bestfriend is: ' + friends;
console.log(bestfriendGreeting);

let currentLocation = 'Arctic Ocean';
const currentLocationGreeting = 'I was found frozen in: ' + currentLocation;
console.log(currentLocationGreeting);
